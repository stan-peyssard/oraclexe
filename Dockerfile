FROM debian:8

MAINTAINER Stanislas Peyssard <stan.peyssard@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive
ENV ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
ENV ORACLE_SID=XE
ENV ORACLE_BASE=/u01/app/oracle
ENV PATH=$ORACLE_HOME/bin:$PATH

ADD chkconfig /sbin/
ADD init.ora /
ADD initXETemp.ora /
ADD oracle-xe_11.2.0-2_amd64.deb.sha1 /

# Install Oracle dependencies
RUN apt-get update && \
	apt-get install libaio1 net-tools bc wget -y && \
	chmod 755 /sbin/chkconfig && \
	ln -s /usr/bin/awk /bin/awk && \
	mkdir /var/lock/subsys && \
	touch /var/lock/subsys/listener && \
	wget https://bitbucket.org/stan-peyssard/oraclexe/raw/006534375ee54d682291ca5e00a29f4797030640/oracle-xe_11.2.0-2_amd64.deb && \
	apt-get autoremove --purge wget  -y && \
	apt-get clean -y && \
	sha1sum --check oracle-xe_11.2.0-2_amd64.deb.sha1 && \
	dpkg --install /oracle-xe_11.2.0-2_amd64.deb && \
    rm -rf oracle-xe* && \
	mv /init.ora /u01/app/oracle/product/11.2.0/xe/config/scripts && \
	mv /initXETemp.ora /u01/app/oracle/product/11.2.0/xe/config/scripts && \
	printf 8080\\n1521\\noracle\\noracle\\ny\\n | /etc/init.d/oracle-xe configure

EXPOSE 1521
EXPOSE 8080

CMD sed -i -E "s/HOST = [^)]+/HOST = $HOSTNAME/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora; \
    /etc/init.d/oracle-xe start; \
	# Workaround to keep docker container up
	while [ 1 ] ; do sleep 60 ; done
	