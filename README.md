oraclexe
============================

Oracle Express Edition 11g Release 2 on Debian 8


Another fork from wnameless/docker-oracle-xe-11g with the package oracle-xe_11.2.0-<b>2</b>_amd64.deb instead of oracle-xe_11.2.0-<b>1</b>_amd64.deb

The purpose is to provide Oracle XE on Debian 8

### Installation

```
docker pull stan0304/oraclexe
```

### Running With Docker
Run with port 1521 opened, optionally expose port 8080 for apex:

```
docker run -d -p 8080:8080 -p 1521:1521 stan0304/oraclexe
```

### Database Connection
Connect database with following setting:

```
hostname: localhost
port: 1521
sid: xe
username: system
password: oracle
```

Password for SYS & SYSTEM

```
oracle
```
### Apex Connection

http://&lt;container_ip&gt;:8080/apex